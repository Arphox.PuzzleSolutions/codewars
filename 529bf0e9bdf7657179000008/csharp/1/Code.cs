    using System.Collections.Generic;
    using System.Linq;

    public class Sudoku
    {
        public static bool ValidateSolution(int[][] board)
        {
            if (board.Any(row => row.Any(item => item == 0)))
            {
                return false;
            }

            if (IsAnyBlockInvalid(board))
                return false;

            if (IsAnyColumnInvalid(board))
                return false;

            return board
                .Select(row => new HashSet<int>(row))
                .All(numbersInARow => numbersInARow.Count == 9);
        }

        private static bool IsAnyBlockInvalid(int[][] board)
        {
            for (int startRow = 0; startRow < board.Length; startRow += 3)
            {
                for (int startCol = 0; startCol < board[startRow].Length; startCol += 3)
                {
                    if (IsBlockInvalidStartingFrom(board, startRow, startCol))
                        return true;
                }
            }

            return false;
        }

        private static bool IsBlockInvalidStartingFrom(int[][] board, int startRow, int startCol)
        {
            HashSet<int> blockItems = new HashSet<int>();

            int toRow = startRow + 3;
            int toCol = startCol + 3;

            for (int row = startRow; row < toRow; row++)
            {
                for (int col = startCol; col < toCol; col++)
                {
                    blockItems.Add(board[row][col]);
                }
            }

            return blockItems.Count != 9;
        }

        private static bool IsAnyColumnInvalid(int[][] board)
        {
            for (int i = 0; i <= 8; i++)
            {
                HashSet<int> column = new HashSet<int>();
                for (int j = 0; j <= 8; j++)
                {
                    column.Add(board[j][i]);
                }

                if (column.Count != 9)
                {
                    return true;
                }
            }

            return false;
        }
    }