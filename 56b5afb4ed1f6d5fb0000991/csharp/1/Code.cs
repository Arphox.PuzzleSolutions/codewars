using System.Linq;
using System.Text;

public class Revrot 
{
    public static string RevRot(string str, int sz)
    {
        if (sz <= 0 || string.IsNullOrEmpty(str) || sz > str.Length)
            return "";

        if (str.Length != sz)
            str = str.Substring(0, (str.Length / sz) * sz); // cut last "stub" part

        StringBuilder result = new StringBuilder(str.Length);
        int from = 0;
        int limit = str.Length;
        while (from < limit)
        {
            string chunk = str.Substring(from, sz);
            string transformed = Transform(chunk);
            result.Append(transformed);
            from += sz;
        }

        return result.ToString();
    }

    private static string Transform(string chunk)
    {
        int itemsSum = chunk.Sum(ch => (int) char.GetNumericValue(ch));
        if (itemsSum % 2 == 0)
            return string.Concat(chunk.Reverse());
        else
            return chunk.Remove(0, 1) + chunk[0];
    }
}