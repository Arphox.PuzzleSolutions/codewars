using System;

public class Line
{
    private static readonly int[] billValues = { 100, 50, 25 };

    public static string Tickets(int[] peopleInLine)
    {
        if (peopleInLine[0] != 25)
            return "NO";

        int[] billAmounts = new int[3]; // amount of: 100, 50, 25

        for (int i = 0; i < peopleInLine.Length; i++)
        {
            int currentMoney = peopleInLine[i];
            int payback = currentMoney - 25;

            bool canPay = TryPay(billAmounts, payback);
            if (!canPay)
                return "NO";

            TakeMoney(billAmounts, currentMoney);
        }

        return "YES";
    }

    private static void TakeMoney(int[] billAmounts, int currentMoney)
    {
        switch (currentMoney)
        {
            case 100: billAmounts[0]++; break;
            case 50: billAmounts[1]++; break;
            case 25: billAmounts[2]++; break;
        }
    }

    private static bool TryPay(int[] billAmounts, int currentMoney)
    {
        int remainingToPay = currentMoney;

        for (int i = 0; i < billAmounts.Length; i++)
        {
            if (remainingToPay >= billValues[i])
            {
                int availableBillCount = Math.Min(remainingToPay / billValues[i], billAmounts[i]);
                billAmounts[i] -= availableBillCount;
                remainingToPay -= availableBillCount * billValues[i];
            }

            if (remainingToPay == 0)
                return true;
        }

        return false;
    }
}