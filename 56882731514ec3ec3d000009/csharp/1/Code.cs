using System;
using System.Collections.Generic;
using System.Linq;

public class ConnectFour
{
    public static void Main()
    {
        List<string> myList = new List<string>()
            {
                "C_Yellow",
                "E_Red",
                "G_Yellow",
                "B_Red",
                "D_Yellow",
                "B_Red",
                "B_Yellow",
                "G_Red",
                "C_Yellow",
                "C_Red",
                "D_Yellow",
                "F_Red",
                "E_Yellow",
                "A_Red",
                "A_Yellow",
                "G_Red",
                "A_Yellow",
                "F_Red",
                "F_Yellow",
                "D_Red",
                "B_Yellow",
                "E_Red",
                "D_Yellow",
                "A_Red",
                "G_Yellow",
                "D_Red",
                "D_Yellow",
                "C_Red"
            };

        Console.WriteLine(WhoIsWinner(myList));


        bool?[,] field = new bool?[6, 7];

        while (true)
        {
            string positionDescriptor = Console.ReadLine();
            PerformStep(positionDescriptor, field);
            GameResult result = GetGameResult(field);
            Console.WriteLine(result);
        }
    }

    private enum GameResult
    {
        Draw = 1,
        Red = 2,
        Yellow = 3
    }

    public static string WhoIsWinner(List<string> piecesPositionList)
    {
        bool?[,] field = new bool?[6, 7];

        foreach (string positionDescriptor in piecesPositionList)
        {
            PerformStep(positionDescriptor, field);

            GameResult result = GetGameResult(field);
            if (result != GameResult.Draw)
                return result.ToString();
        }

        return GameResult.Draw.ToString();
    }

    private static GameResult GetGameResult(bool?[,] field)
    {
        GameResult result;

        // Rows
        for (int rowIndex = 0; rowIndex < field.GetLength(0); rowIndex++)
        {
            bool?[] selectedRow = field.SelectRow(rowIndex);
            result = CheckArray(selectedRow);
            if (result != GameResult.Draw)
                return result;
        }

        // Columns
        for (int colIndex = 0; colIndex < field.GetLength(1); colIndex++)
        {
            bool?[] selectedCol = field.SelectColumn(colIndex);
            result = CheckArray(selectedCol);
            if (result != GameResult.Draw)
                return result;
        }

        // Diagonals Right-Down
        for (int rowIndex = field.GetLength(0) - 4; rowIndex >= 0; rowIndex--)
        {
            bool?[] selectedElements = field.SelectDiagRightDown(rowIndex, 0);
            result = CheckArray(selectedElements);
            if (result != GameResult.Draw)
                return result;
        }
        for (int colIndex = 1; colIndex < field.GetLength(1) - 4; colIndex++)
        {
            bool?[] selectedElements = field.SelectDiagRightDown(0, colIndex);
            result = CheckArray(selectedElements);
            if (result != GameResult.Draw)
                return result;
        }

        // Diagonals Left-Down
        for (int rowIndex = field.GetLength(0) - 4; rowIndex >= 0; rowIndex--)
        {
            bool?[] selectedElements = field.SelectDiagLeftDown(rowIndex, field.GetLength(1) - 1);
            result = CheckArray(selectedElements);
            if (result != GameResult.Draw)
                return result;
        }
        for (int colIndex = field.GetLength(1) - 1; colIndex > 3; colIndex--)
        {
            bool?[] selectedElements = field.SelectDiagLeftDown(field.GetLength(0) - 1, colIndex);
            result = CheckArray(selectedElements);
            if (result != GameResult.Draw)
                return result;
        }

        return GameResult.Draw;
    }

    private static GameResult CheckArray(bool?[] selectedRow)
    {
        int max = selectedRow.Length - 4;
        for (int i = 0; i <= max; i++)
        {
            var subRow = selectedRow.Skip(i).Take(4);
            if (subRow.Count(x => x == true) == 4)
                return GameResult.Yellow;
            else if (subRow.Count(x => x == false) == 4)
                return GameResult.Red;
        }

        return GameResult.Draw;
    }

    private static void PerformStep(string positionDescriptor, bool?[,] field)
    {
        string[] splitted = positionDescriptor.Split('_');
        int col = splitted[0][0] - 'A';
        bool player = GetPlayer(splitted[1]);

        for (int i = field.GetLength(0) - 1; i >= 0; i--)
        {
            if (field[i, col] == null)
            {
                field[i, col] = player;
                return;
            }
        }

        throw new ArgumentException($"Incorrect step: Player {GetPlayerColor(player)} cannot put to column {col} because it is full.");
    }


    private static bool GetPlayer(string playerRaw) => playerRaw == "Yellow" ? true : false;
    private static string GetPlayerColor(bool player) => player ? "Yellow" : "Red";
    private static GameResult GetPlayerEnum(bool player) => player ? GameResult.Yellow : GameResult.Red;
}

internal static class TwoDimensionalArrayExtensions
{
    internal static bool?[] SelectRow(this bool?[,] array, int rowIndex)
    {
        bool?[] output = new bool?[array.GetLength(1)];
        for (int col = 0; col < output.Length; col++)
        {
            output[col] = array[rowIndex, col];
        }
        return output;
    }

    internal static bool?[] SelectColumn(this bool?[,] array, int colIndex)
    {
        bool?[] output = new bool?[array.GetLength(0)];
        for (int row = 0; row < output.Length; row++)
        {
            output[row] = array[row, colIndex];
        }
        return output;
    }

    internal static bool?[] SelectDiagRightDown(this bool?[,] array, int rowIndex, int colIndex)
    {
        List<bool?> output = new List<bool?>();

        while (array.IsCorrectIndex(rowIndex, colIndex))
        {
            output.Add(array[rowIndex, colIndex]);
            rowIndex++;
            colIndex++;
        }

        return output.ToArray();
    }

    internal static bool?[] SelectDiagLeftDown(this bool?[,] array, int rowIndex, int colIndex)
    {
        List<bool?> output = new List<bool?>();

        while (array.IsCorrectIndex(rowIndex, colIndex))
        {
            output.Add(array[rowIndex, colIndex]);
            rowIndex++;
            colIndex--;
        }

        return output.ToArray();
    }

    internal static bool IsCorrectIndex(this bool?[,] array, int rowIndex, int colIndex)
    {
        return rowIndex >= 0 && rowIndex < array.GetLength(0) && colIndex >= 0 && colIndex < array.GetLength(1);
    }

    internal static bool Any(this bool?[,] array, bool? element)
    {
        for (int i = 0; i < array.GetLength(0); i++)
        {
            for (int j = 0; j < array.GetLength(1); j++)
            {
                if (array[i, j].Equals(element))
                    return true;
            }
        }

        return false;
    }
}