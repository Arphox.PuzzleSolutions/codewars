public class Kata
{
    public static bool Solution(string str, string ending)
    {
        return str.EndsWith(ending);
    }

    public static bool Solution_HandWritten(string str, string ending)
    {
        if (string.IsNullOrEmpty(str) || ending == null)
            return false;
        if (ending == string.Empty)
            return true;

        int endingIndexer = ending.Length - 1;
        int i;
        for (i = str.Length - 1; i >= 0; i--)
        {
            if (endingIndexer < 0)
                return true;

            if (str[i] != ending[endingIndexer])
                return false;

            endingIndexer--;
        }

        return endingIndexer < 0;
    }
}