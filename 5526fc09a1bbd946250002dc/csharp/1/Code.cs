using System;
using System.Linq;

public class Kata
{
    public static int Find(int[] integers)
    {
        int oddCount = integers.Count(i => i % 2 == 1);
        if (oddCount == 1)
            return Array.Find(integers, i => i % 2 == 1);

        return Array.Find(integers, i => i % 2 == 0);
    }
}