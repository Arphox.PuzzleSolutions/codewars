using System;

public class Kata
{
  public static bool IsSquare(int n)
  {
    return (Math.Sqrt(n) - (int)Math.Sqrt(n)) == 0;
  }
}