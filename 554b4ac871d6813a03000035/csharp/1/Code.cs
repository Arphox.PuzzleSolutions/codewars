using System.Linq;

public static class Kata
{
    public static string HighAndLow(string rawNumbers)
    {
        string[] numbers = rawNumbers.Split(' ');
        int min = numbers.Min(int.Parse);
        int max = numbers.Max(int.Parse);
        return $"{max} {min}";
    }
}