public class Kata
{
    public static string Rgb(int r, int g, int b)
    {
        return ToRGBhex(r) + ToRGBhex(g) + ToRGBhex(b);
    }

    private static string ToRGBhex(int number)
    {
        number = ScaleBetween0And255(number);
        return number.ToString("X").PadLeft(2, '0');
    }

    private static int ScaleBetween0And255(int number)
    {
        if (number < 0)
            number = 0;
        else if (number > 255)
            number = 255;

        return number;
    }
}