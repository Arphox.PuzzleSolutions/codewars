public class Kata
{
    public static string ToCamelCase(string str)
    {
        string[] parts = str.Split('-', '_');

        for (int i = 1; i < parts.Length; i++)
        {
            parts[i] = char.ToUpper(parts[i][0]) + parts[i].Substring(1);
        }

        return string.Concat(parts);
    }
}