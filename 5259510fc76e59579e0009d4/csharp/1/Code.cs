using System;
using System.Collections.Generic;
using System.Linq;

public class Kata
{
    private List<string> _words;

    public Kata(IEnumerable<string> words)
    {
        _words = words.ToList();
    }

    public string FindMostSimilar(string term)
    {
        int[] distances = _words.Select(w => ComputeLevenshteinDistance(w, term)).ToArray();

        int minIndex = 0;
        for (int i = 1; i < distances.Length; i++)
            if (distances[i] < distances[minIndex])
                minIndex = i;

        return _words[minIndex];
    }

    private static int ComputeLevenshteinDistance(string string1, string string2)
    {
        int n = string1.Length;
        int m = string2.Length;
        int[,] d = new int[n + 1, m + 1];

        if (n == 0)
            return m;
        if (m == 0)
            return n;

        for (int i = 0; i <= n; d[i, 0] = i++) { }
        for (int j = 0; j <= m; d[0, j] = j++) { }

        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                int cost = (string2[j - 1] == string1[i - 1]) ? 0 : 1;

                d[i, j] = Math.Min(
                    Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                    d[i - 1, j - 1] + cost);
            }
        }
        return d[n, m];
    }
}