using System;

public class Kata
{
    private const int SubSize = 5;
    public static int GetNumber(string str)
    {
        if (str.Length < SubSize)
            throw new ArgumentOutOfRangeException(nameof(str), $"The string has to contain at least {SubSize} elements.");
        if (str.Length == SubSize)
            return int.Parse(str);

        int max = 0;
        for (int i = 0; i <= str.Length - SubSize; i++)
        {
            int num = int.Parse(str.Substring(i, SubSize));
            if (num > max)
                max = num;
        }

        return max;
    }
}