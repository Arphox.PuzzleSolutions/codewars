﻿namespace Calculator
{
    using System;
    using System.Collections.Generic;

    internal sealed class UncheckedSimpleCalculator
    {
        private Stack<double> valueStack = new Stack<double>();
        private Stack<char> operatorStack = new Stack<char>();

        internal void AddOperand(int operand)
        {
            valueStack.Push(operand);
        }

        internal void AddOperator(char addedOperator)
        {
            while (IsThereAnyOperator && MostRecentlyAddedOperator.IsNotSmallerPrecedenceThan(addedOperator))
                ProcessStacksOnce();

            operatorStack.Push(addedOperator);
        }

        internal double CalculateResult()
        {
            while (IsThereAnyOperator)
                ProcessStacksOnce();

            return valueStack.Pop();
        }

        private void ProcessStacksOnce()
        {
            char lastOperator = operatorStack.Pop();
            double rightOperand = valueStack.Pop();
            double leftOperand = valueStack.Pop();
            double newValue = ApplyOperator(lastOperator, leftOperand, rightOperand);
            valueStack.Push(newValue);
        }

        private double ApplyOperator(char op, double leftOperand, double rightOperand)
        {
            switch (op)
            {
                case '+': return leftOperand + rightOperand;
                case '-': return leftOperand - rightOperand;
                case '*': return leftOperand * rightOperand;
                case '/': return leftOperand / rightOperand;
            }

            throw new InvalidOperationException($"Unsupported operator: '{op}'");
        }

        private bool IsThereAnyOperator => operatorStack.Count > 0;
        private char MostRecentlyAddedOperator => operatorStack.Peek();
    }
}