﻿using System;

namespace Calculator
{
    internal static class CharExtensions
    {
        internal static bool IsNotSmallerPrecedenceThan(this char op1, char op2)
        {
            int op1prec = GetPrecedenceOf(op1);
            int op2prec = GetPrecedenceOf(op2);

            return op1prec >= op2prec;
        }

        private static int GetPrecedenceOf(char op)
        {
            switch (op)
            {
                case '(':
                case ')': return 2;
                case '*':
                case '/': return 1;
                case '+':
                case '-': return 0;
            }

            throw new InvalidOperationException();
        }

        internal static bool IsOperator(this char ch)
        {
            return ch == '*' || ch == '/' || ch == '+' || ch == '-' || ch == '(' || ch == ')';
        }
    }
}