using System;
using System.Linq;

public class Kata
{
    public static bool Narcissistic(int value)
    {
        string valueAsString = value.ToString();
        int numberOfDigits = valueAsString.Length;

        long[] values = valueAsString.Select(x => PowChar(x, numberOfDigits)).ToArray();

        return values.Sum() == value;
    }

    private static long PowChar(char x, int power)
    {
        int numericValue = (int) char.GetNumericValue(x);
        return (long)Math.Pow(numericValue, power);
    }
}