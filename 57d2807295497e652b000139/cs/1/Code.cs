public class Kata
{
  public static double[] Averages(int[] numbers)
  {
    if(numbers == null || numbers.Length <= 1) return new double[0];
    
    double[] averages = new double[numbers.Length - 1];
    
    for(int i = 0; i < numbers.Length - 1; i++)
    {
        averages[i] = (numbers[i] + numbers[i + 1]) / 2.0;
    }
    
    return averages;
  }
}