using System.Linq;

public static class Kata
{
    public static string AlphabetPosition(string text)
    {
        var positions = text.ToLower().Select(ch => GetPosition(ch)).Where(pos => pos != 0);
        return string.Join(" ", positions);
    }

    private static int GetPosition(char ch)
    {
        if (ch > 'z' || ch < 'a')
            return 0;
        else
            return ch - 'a' + 1;
    }
}