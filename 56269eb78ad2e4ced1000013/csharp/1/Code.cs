using System;

public class Kata
{
    public static long FindNextSquare(long num)
    {
        if (!IsPerfectSquare(num))
            return -1;

        return (long)Math.Pow(Math.Sqrt(num) + 1, 2);
    }

    private static bool IsPerfectSquare(long num)
    {
        return Math.Sqrt(num) % 1 == 0;
    }
}