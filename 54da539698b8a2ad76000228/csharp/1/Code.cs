using System;

public class Kata
{
    private const int WalkSize = 10;
    public static bool IsValidWalk(string[] walk)
    {
        if (walk.Length != WalkSize)
            return false;

        int x = 0;
        int y = 0;
        foreach (string step in walk)
        {
            switch (step)
            {
                case "n": y++; break;
                case "s": y--; break;
                case "e": x++; break;
                case "w": x--; break;
                default:
                    throw new ArgumentNullException(nameof(walk), $"Unexpected step character of `{step}`");
            }
        }
        return x == 0 && y == 0;
    }
}