using System;

public static class TimeFormat
{
    public static string GetReadableTime(int seconds)
    {
        if (seconds < 0 || seconds > 359999)
            throw new ArgumentOutOfRangeException(nameof(seconds), "The parameter has to be between 0 and 359999.");

        int hours = seconds / 3600;
        seconds = seconds % 3600;

        int minutes = seconds / 60;
        seconds = seconds % 60;

        return $"{hours:D2}:{minutes:D2}:{seconds:D2}";
    }
}