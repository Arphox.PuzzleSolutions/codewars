using System.Linq;
public static class Kata
{
    public static bool XO(string input)
    {
        int x = input.Count(ch => ch == 'x' || ch == 'X');
        int o = input.Count(ch => ch == 'o' || ch == 'O');
        return x == o;
  }
}