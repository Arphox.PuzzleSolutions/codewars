﻿namespace FluentCalculator.Tests.UncheckedSimpleCalculator
{
    internal static class UncheckedSimpleCalculatorTestHelper
    {
        internal static FluentCalculatorNamespace.UncheckedSimpleCalculator Parse(string expression)
        {
            var calc = new FluentCalculatorNamespace.UncheckedSimpleCalculator();

            string[] parts = expression.Split(' ');
            foreach (string part in parts)
            {
                if (int.TryParse(part, out int number))
                    calc.AddOperand(number);
                else
                    calc.AddOperator(part[0]);
            }

            return calc;
        }
    }
}