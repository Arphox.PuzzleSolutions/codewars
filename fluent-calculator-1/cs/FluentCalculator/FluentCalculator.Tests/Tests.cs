﻿namespace Kata
{
    using NUnit.Framework;

    [TestFixture]
    public class Tests
    {
        [Test]
        public static void BasicAddition()
        {
            var calculator = new FluentCalculator();

            //Test Result Call
            Assert.AreEqual(3, calculator.One.Plus.Two.Result());
        }

        [Test]
        public static void MultipleInstances()
        {
            var calculatorOne = new FluentCalculator();
            var calculatorTwo = new FluentCalculator();

            double a = (double)calculatorOne.Five.Plus.Five;
            double b = (double)calculatorTwo.Seven.Times.Three;
            Assert.AreNotEqual(a, b);
        }

        [Test]
        public static void MultipleCalls()
        {
            //Testing that the expression or reference clears between calls
            var calculator = new FluentCalculator();
            Assert.AreEqual(4, calculator.One.Plus.One.Result() + calculator.One.Plus.One.Result());
        }

        [Test]
        public static void SimpleBedmas()
        {
            //Testing Order of Operations
            var calculator = new FluentCalculator();
            Assert.AreEqual(8, (double)calculator.Eight.DividedBy.Two.Times.Two);
        }

        [Test]
        public static void Bedmas()
        {
            //Testing Order of Operations
            var calculator = new FluentCalculator();
            Assert.AreEqual(58, (double)calculator.Six.Times.Six.Plus.Eight.DividedBy.Two.Times.Two.Plus.Ten.Times.Four.DividedBy.Two.Minus.Six);
            Assert.AreEqual(-11.972, calculator.Zero.Minus.Four.Times.Three.Plus.Two.DividedBy.Eight.Times.One.DividedBy.Nine, 0.01);
        }

        [Test]
        public static void StaticCombinationCalls()
        {
            //Testing Implicit Conversions
            var calculator = new FluentCalculator();
            Assert.AreEqual(177.5, 10 * calculator.Six.Plus.Four.Times.Three.Minus.Two.DividedBy.Eight.Times.One.Minus.Five.Times.Zero);
        }
    }
}