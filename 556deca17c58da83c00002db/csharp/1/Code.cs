public class Xbonacci
{
    public double[] Tribonacci(double[] signature, int n)
    {
        if (n == 0) return new double[0];
        if (n == 1) return new double[] { signature[0] };
        if (n == 2) return new double[] { signature[0], signature[1] };
        if (n == 3) return new double[] { signature[0], signature[1], signature[2] };

        double[] tribonacci = new double[n];
        tribonacci[0] = signature[0];
        tribonacci[1] = signature[1];
        tribonacci[2] = signature[2];

        for (int i = 3; i < n; i++)
        {
            tribonacci[i] = tribonacci[i - 1] + tribonacci[i - 2] + tribonacci[i - 3];
        }

        return tribonacci;
    }
}