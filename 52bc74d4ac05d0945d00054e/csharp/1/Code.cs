    using System.Collections.Generic;

    public static class Kata
    {
        public static string FirstNonRepeatingLetter(string input)
        {
            HashSet<char> alreadyCheckedChars = new HashSet<char>();

            for (int i = 0; i < input.Length; i++)
            {
                char current = input[i];
                char currentToCompare = char.ToLower(current);
                if (alreadyCheckedChars.Contains(currentToCompare))
                    continue;

                if (i == input.Length - 1)
                    return current.ToString();

                string rest = input.Substring(i + 1, input.Length - i - 1).ToLower();
                if (!rest.Contains(currentToCompare.ToString()))
                {
                    return current.ToString();
                }

                alreadyCheckedChars.Add(currentToCompare);
            }

            return "";
        }
    }