using System.Collections.Generic;
using System.Linq;

class WhichAreIn
{
    public static string[] inArray(string[] array1, string[] array2)
    {
        var results = new HashSet<string>();

        foreach (string a1 in array1)
        {
            foreach (string a2 in array2)
            {
                if (a2.Contains(a1))
                    results.Add(a1);
            }
        }

        return results.OrderBy(x => x).ToArray();
    }
}