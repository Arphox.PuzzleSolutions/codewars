using System;

public static class Kata
{
    public static bool IsPrime(int n)
    {
        if (n < 2) return false;

        int limit = (int)Math.Sqrt(n) + 1;
        for (int i = 2; i < limit; i++)
            if (n % i == 0)
                return false;

        return true;
    }
}