using System;
using System.Linq;

public class Dubstep
    {
        public static string SongDecoder(string input)
        { 
           string[] words = input.Split(new [] { "WUB" }, StringSplitOptions.RemoveEmptyEntries);
           return string.Join(" ", words);
        }
    }