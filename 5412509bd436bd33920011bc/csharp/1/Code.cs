public static class Kata
{
  // return masked string
  public static string Maskify(string cc)
  {
      if(cc.Length <= 4)
        return cc;
      else
        return new string('#', cc.Length - 4) + cc.Substring(cc.Length - 4, 4);
  }
}