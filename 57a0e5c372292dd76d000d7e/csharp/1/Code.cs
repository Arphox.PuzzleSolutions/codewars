using System.Text;

namespace Solution
{
    public static class Program
    {
        public static string repeatStr(int n, string s)
        {
            StringBuilder output = new StringBuilder(n * s.Length);
            for (int i = 0; i < n; i++)
                output.Append(s);
            return output.ToString();
        }
    }
}