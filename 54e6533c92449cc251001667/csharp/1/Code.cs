using System.Collections.Generic;
using System.Linq;

public static class Kata
{
    public static IEnumerable<T> UniqueInOrder<T>(IEnumerable<T> iterable)
    {
        if (iterable == null)
            return null;
        if (!iterable.Any())
            return Enumerable.Empty<T>();

        List<T> output = new List<T>();
        output.Add(iterable.First());
        T lastItem = output[0];

        foreach (T item in iterable)
        {
            if (!lastItem.Equals(item))
            {
                output.Add(item);
                lastItem = item;
            }
        }

        return output;
    }
}