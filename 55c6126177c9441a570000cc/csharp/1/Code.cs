using System.Linq;

public static class WeightSort
{
    public static string orderWeight(string s)
    {
        var result = s.Split(' ')
            .OrderBy(n => n.Select(c => (int)char.GetNumericValue(c)).Sum())
            .ThenBy(n => n);

        return string.Join(" ", result);
    }
}