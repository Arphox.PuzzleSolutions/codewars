public static class Kata
{
    public static long rowSumOddNumbers(long n)
    {
        long current = n * (n - 1) + 1;
        long sum = 0;

        for (long i = 0; i < n; i++)
        {
            sum += current;
            current += 2;
        }

        return sum;
    }
}