using System.Linq;

public static class Kata
{
    public static int DescendingOrder(int num)
    {
        var digits = num.ToString().OrderByDescending(ch => char.GetNumericValue(ch));
        return int.Parse(string.Concat(digits));
    }
}