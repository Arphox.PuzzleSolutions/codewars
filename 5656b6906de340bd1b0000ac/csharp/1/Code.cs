using System.Linq;

public class TwoToOne
{
    public static string Longest(string s1, string s2)
    {
        char[] merged = s1.Union(s2)
            .OrderBy(x => x)
            .ToArray();

        return new string(merged);
    }
}