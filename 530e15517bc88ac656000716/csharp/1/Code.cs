using System.Linq;

public class Kata
{
    public static string Rot13(string message)
    {
        return new string(message.ToCharArray().Select(Shift).ToArray());
    }

    private static char Shift(char ch)
    {
        bool isUpperCase = char.IsUpper(ch);
        ch = char.ToLower(ch);

        if (ch < 'a' || ch > 'z')
            return ch;

        ch = (char)(ch + 13);
        if (ch > 'z')
            ch -= (char)('z' - 'a' + 1);

        if (isUpperCase)
            ch = char.ToUpper(ch);

        return ch;
    }
}