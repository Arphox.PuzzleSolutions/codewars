using System.Linq;

namespace Solution
{
    public class Kata
    {
        public static bool is_valid_IP(string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(ipAddress) || ipAddress.Contains(' '))
                return false;

            string[] stringParts = ipAddress.Split('.');
            if (stringParts.Length != 4)
                return false;

            int[] numbers = new int[4];
            for (int i = 0; i < 4; i++)
            {
                int result;
                bool canParse = int.TryParse(stringParts[i], out result);
                if (!canParse || stringParts[i].StartsWith("0") && result != 0)
                    return false;

                numbers[i] = result;
            }

            return numbers.All(n => n >= 0 && n <= 255);
        }
    }
}