using System;

public static class Kata
{
    public static int sumTwoSmallestNumbers(int[] numbers)
    {
        if (numbers.Length < 4)
            throw new ArgumentOutOfRangeException(nameof(numbers), "The array should have at least 4 elements.");

        int least = int.MaxValue;
        int secondLeast = int.MaxValue;
        foreach (int num in numbers)
        {
            if (num < 1)
                continue;

            if (num < secondLeast)
            {
                if (num < least)
                {
                    secondLeast = least;
                    least = num;
                }
                else
                {
                    secondLeast = num;
                }
            }
        }

        return least + secondLeast;
    }
}