using System;
using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static int GetUnique(IEnumerable<int> numbers)
    {
        int x = numbers.ElementAt(0);

        int y = int.MinValue;
        IEnumerator<int> enumerator = numbers.GetEnumerator();
        while (enumerator.MoveNext())
        {
            int current = enumerator.Current;
            if (current != x)
            {
                y = current;
                break;
            }
        }

        if (enumerator.MoveNext())
        {
            int current = enumerator.Current;
            return current == x ? y : x;
        }

        throw new ArgumentException("The input is invalid.");
    }
}