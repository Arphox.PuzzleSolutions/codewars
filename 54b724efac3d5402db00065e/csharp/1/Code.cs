using System;
using System.Linq;

class MorseCodeDecoder
{
    public static string Decode(string morseCode)
    {
        string[] words = morseCode.Split(new[] { "   " }, StringSplitOptions.RemoveEmptyEntries);
        return string.Join(" ", words.Select(DecodeWord));
    }

    private static string DecodeWord(string morseWord)
    {
        string[] lettersInMorse = morseWord.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        return string.Concat(lettersInMorse.Select(MorseCode.Get));
    }
}