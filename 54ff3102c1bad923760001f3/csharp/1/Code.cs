using System.Linq;

public static class Kata
{
    public static int GetVowelCount(string str)
    {
        char[] vowels = { 'a', 'e', 'i', 'o', 'u' };

        return str.Count(s => vowels.Contains(s));
    }
}