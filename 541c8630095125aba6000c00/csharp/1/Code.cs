using System.Linq;

public class Number
{
    public int DigitalRoot(long n)
    {
        if (n < 10)
            return (int)n;

        int[] numbers = SplitIntoDigits(n);
        return DigitalRootInternal(numbers);
    }

    private int DigitalRootInternal(int[] numbers)
    {
        int sum = numbers.Sum();
        if (sum < 10)
        {
            return sum;
        }
        else
        {
            return DigitalRootInternal(SplitIntoDigits(sum));
        }
    }

    private int[] SplitIntoDigits(long number)
    {
        return number.ToString().Select(ch => (int)char.GetNumericValue(ch)).ToArray();
    }
}