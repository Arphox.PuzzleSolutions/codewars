using System.Numerics;

public static class Kata
{
    public static string sumStrings(string a, string b)
    {
        BigInteger aNum = Parse(a);
        BigInteger bNum = Parse(b);
        return (aNum + bNum).ToString();
    }

    private static BigInteger Parse(string x)
    {
        if (string.IsNullOrEmpty(x))
            return BigInteger.Zero;
        else
            return BigInteger.Parse(x);
    }
}